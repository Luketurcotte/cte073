#Health System Integration 

##Course Topics:
- Defining Integrated Care
- Risk-Sharing Models in Health Care
	- Bundled Payments
	- Shared Savings
	- Pay-for-Performance
- Integrated Service Models
- Patient Flow in the Canadian Health System
- Measuring System Integration
- Barriers to Integrated Care
 
 
